/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package usfx.zoologico;

/**
 *
 * @author micky
 */
public class Mamiferos extends Animal {
    private String nombre;
    private double temperatura;
    private int numeroPatas;
    private String color;

    public Mamiferos(String nombre, double temperatura, int numeroPatas, String color) {
        super(nombre);
        this.temperatura = temperatura;
        this.numeroPatas = numeroPatas;
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public int getNumeroPatas() {
        return numeroPatas;
    }

    public void setNumeroPatas(int numeroPatas) {
        this.numeroPatas = numeroPatas;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    

        @Override
    public String obtenerInformacion() {
        return "Mamífero: " + getNombre() + ", Temperatura: " + temperatura + ", Patas: " + numeroPatas + ", Color: " + color;
    }

}
