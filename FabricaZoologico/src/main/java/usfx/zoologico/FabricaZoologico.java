/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package usfx.zoologico;

/**
 *
 * @author micky
 */
public class FabricaZoologico  {
    public static void main(String[] args) {
        // Crear animales
        Mamiferos leon = new Mamiferos("Leon", 32.5, 4, "Amarillo");
        Aves loro = new Aves("Loro", 0.5, 0.3);
        Peces pacu = new Peces("Pacu", 0.2);

        // Crear jaulas
        Jaula jaula1 = new Jaula(leon, 2.0, 3.0, 2.0);
        Jaula jaula2 = new Jaula(loro, 1.0, 1.0, 1.0);
        Jaula jaula3 = new Jaula(pacu, 0.5, 0.5, 0.5);

        // Crear zoológico
        Zoologico zoo = new Zoologico("Zoológico");
        zoo.agregarJaula(jaula1);
        zoo.agregarJaula(jaula2);
        zoo.agregarJaula(jaula3);

        // Mostrar animales en el zoológico
        zoo.mostrarMamiferos();
        zoo.mostrarAves();
        zoo.mostrarPeces();
    }
}