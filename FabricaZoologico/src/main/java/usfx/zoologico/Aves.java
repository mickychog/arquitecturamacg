/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package usfx.zoologico;

/**
 *
 * @author micky
 */
public class Aves extends Animal{
    private String nombre;
    private double peso;
    private double tamañoAlas;

    public Aves(String nombre, double peso, double tamañoAlas) {
        super(nombre);
        this.peso = peso;
        this.tamañoAlas = tamañoAlas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getTamañoAlas() {
        return tamañoAlas;
    }

    public void setTamañoAlas(double tamañoAlas) {
        this.tamañoAlas = tamañoAlas;
    }

    @Override
    public String obtenerInformacion() {
        return "Ave: " + getNombre() + ", Peso: " + peso + ", Tamaño de Alas: " + tamañoAlas;
    }

   
    
}
