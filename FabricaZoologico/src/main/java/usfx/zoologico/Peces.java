/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package usfx.zoologico;

/**
 *
 * @author micky
 */
public class Peces extends Animal{
    private String nombre;
    private double longitud;

    public Peces(String nombre, double longitud) {
        super(nombre);
        this.longitud = longitud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String obtenerInformacion() {
        return "Pez: " + getNombre() + ", Longitud: " + longitud;
    }

}
