/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package usfx.zoologico;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author micky
 */
public class Zoologico {
    private String nombre;
    private List<Jaula> listaDeJaulas = new ArrayList<>();

    public Zoologico(String nombre) {
        this.nombre = nombre;
    }

    public void agregarJaula(Jaula jaula) {
        listaDeJaulas.add(jaula);
    }

    public void mostrarMamiferos() {
        System.out.println("Mamíferos en el Zoológico " + nombre + ":");
        for (Jaula jaula : listaDeJaulas) {
            if (jaula.getAnimal() instanceof Mamiferos) {
                System.out.println(jaula);
            }
        }
    }

    public void mostrarAves() {
        System.out.println("Aves en el Zoológico " + nombre + ":");
        for (Jaula jaula : listaDeJaulas) {
            if (jaula.getAnimal() instanceof Aves) {
                System.out.println(jaula);
            }
        }
    }

    public void mostrarPeces() {
        System.out.println("Peces en el Zoológico " + nombre + ":");
        for (Jaula jaula : listaDeJaulas) {
            if (jaula.getAnimal() instanceof Peces) {
                System.out.println(jaula);
            }
        }
    }
    
}
